using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ScriptableAction : ScriptableObject
{
    public abstract void OnFinishedState();

    public abstract void OnSetState(StateController sc);

    public abstract void OnUpdate(StateController sc);
}
