using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "ScriptableRun", menuName = "ScriptableObjects2/ScriptableAction/ScriptableRun")]

public class ScriptableRun : ScriptableAction
{
    public override void OnFinishedState()
    {
        GameManager.gm.UpdateText("estoy huyendo");
    }

    public override void OnSetState(StateController sc)
    {
        GameManager.gm.UpdateText("estoy huyendo");
    }

    public override void OnUpdate(StateController sc)
    {
        GameManager.gm.UpdateText("estoy huyendo");
    }

}
