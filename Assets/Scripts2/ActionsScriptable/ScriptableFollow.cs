using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "ScriptableFollow", menuName = "ScriptableObjects2/ScriptableAction/ScriptableFollow")]

public class ScriptableFollow : ScriptableAction
{
    private ChaseBehaviour _chaseBehaviour;
    private EnemyController2 _enemyController;
    public override void OnFinishedState()
    {
        GameManager.gm.UpdateText("ya no te persigo");
        _chaseBehaviour.StopChasing();
    }

    public override void OnSetState(StateController sc)
    {
        GameManager.gm.UpdateText("Te persigo");
        _chaseBehaviour = sc.GetComponent<ChaseBehaviour>();
        _enemyController = (EnemyController2)sc;
    }

    public override void OnUpdate(StateController sc)
    {
        _chaseBehaviour.Chase(_enemyController.target.transform, sc.transform);
    }
}
